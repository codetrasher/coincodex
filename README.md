# CoinCodex
[![Build status](https://ci.appveyor.com/api/projects/status/i34dkigo2q0nw8i0/branch/master?svg=true)](https://ci.appveyor.com/project/codetrasher/coincodex/branch/master)

This package is a C# wrapper for a cryptocurrency market API provided by https://coincodex.com. All rights to the actual API belong to them.