﻿using System.Collections.Specialized;
using System.Configuration;

namespace CoinCodex
{
    internal class ConfigReader : IConfigReader
    {
        public NameValueCollection ReadConfig()
        {
            return ConfigurationManager.AppSettings;
        }
    }

    public interface IConfigReader
    {
        NameValueCollection ReadConfig();
    }
}
