﻿using Newtonsoft.Json;
using System;

namespace CoinCodex.Converters
{
    public class StringToBoolConverter : JsonConverter
    {
        private const string TRUE = "true";

        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = reader.Value;

            if (value == null)
            {
                return false;
            }

            if (Equals(TRUE, value))
            {
                return true;
            }

            return false;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
