﻿using CoinCodex.Converters;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CoinCodex.Model
{
    public class Coin
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("display_symbol")]
        public string DisplaySymbol { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("aliases")]
        public string Aliases { get; set; }
        [JsonProperty("shortname")]
        public string ShortName { get; set; }
        [JsonProperty("last_price_usd")]
        public decimal? LastPriceUsd { get; set; }
        [JsonProperty("market_cap_rank")]
        public int? MarketCapRank { get; set; }
        [JsonProperty("volume_rank")]
        public int? VolumeRank { get; set; }
        [JsonProperty("price_change_1H_percent")]
        public double? PriceChangeOneHourPercent { get; set; }
        [JsonProperty("price_change_1D_percent")]
        public double? PriceChangeOneDayPercent { get; set; }
        [JsonProperty("price_change_7D_percent")]
        public double? PriceChangeOneWeekDPercent { get; set; }
        [JsonProperty("price_change_30D_percent")]
        public double? PriceChangeOneMonthPercent { get; set; }
        [JsonProperty("price_change_90D_percent")]
        public double? PriceChangeThreeMonthsPercent { get; set; }
        [JsonProperty("price_change_180D_percent")]
        public double? PriceChangeSixMonthsPercent { get; set; }
        [JsonProperty("price_change_365D_percent")]
        public double? PriceChangeOneYearPercent { get; set; }
        [JsonProperty("price_change_3Y_percent")]
        public double? PriceChangeThreeYearsPercent { get; set; }
        [JsonProperty("price_change_5Y_percent")]
        public double? PriceChangeFiveYearsPercent { get; set; }
        [JsonProperty("price_change_ALL_percent")]
        public double? PriceChangeAllPercent { get; set; }
        [JsonProperty("price_change_YTD_percent")]
        public double? PriceChangeYTDPercent { get; set; }
        [JsonProperty("volume_24_usd")]
        public double? Volume24HUsd { get; set; }
        [JsonProperty("display")]
        [JsonConverter(typeof(StringToBoolConverter))]
        public bool Display { get; set; }
        [JsonProperty("trading_since")]
        public string TradingSince { get; set; }
        [JsonProperty("supply")]
        public double? Supply { get; set; }
        [JsonProperty("last_update")]
        public string? LastUpdate { get; set; }
        [JsonProperty("ico_end")]
        public string? IcoEnd { get; set; }
        [JsonProperty("include_supply")]
        public bool IncludeSupply { get; set; }
        [JsonProperty("use_volume")]
        public bool UseVolume { get; set; }
        [JsonProperty("growth_all_time")]
        public double? GrowthAllTime { get; set; }
        [JsonProperty("ccu_slug")]
        public string CCUSlug { get; set; }
        [JsonProperty("image_t")]
        public int? ImageT { get; set; }
        [JsonProperty("market_cap_usd")]
        public decimal? MarketCapUsd { get; set; }
        [JsonProperty("categories")]
        public IEnumerable<int> Categories { get; set; }
    }
}
