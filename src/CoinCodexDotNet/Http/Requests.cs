﻿using System;
using System.Collections.Specialized;
using System.Net.Http;

namespace CoinCodex.Http
{
    internal static class Requests
    {
        public static HttpRequestMessage CreateGetAllCoinsRequest(NameValueCollection settings)
        {
            Uri getAllCoins = new Uri(new Uri(settings["BaseUrl"].ToString()), settings["GetAllCoinsUrl"].ToString());
            return new(HttpMethod.Get, getAllCoins);
        }
    }
}
