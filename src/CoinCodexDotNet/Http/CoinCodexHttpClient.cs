﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CoinCodex.Model;
using Newtonsoft.Json;

namespace CoinCodex.Http
{
    public class CoinCodexHttpClient
    {
        private readonly HttpClient _client;
        private readonly CancellationTokenSource _cancelSource;
        private readonly NameValueCollection _appSettings;

        public CoinCodexHttpClient(HttpClient client = null, CancellationTokenSource source = null, IConfigReader configReader = null)
        {
            _client = client ?? new HttpClient();
            _cancelSource = source ?? new CancellationTokenSource();
            var confReader = configReader ?? new ConfigReader();
            _appSettings = confReader.ReadConfig();
        }

        public async Task<IEnumerable<Coin>> GetAllCoinsAsync()
        {
            var response = await _client.SendAsync(Requests.CreateGetAllCoinsRequest(_appSettings), _cancelSource.Token);
            return JsonConvert.DeserializeObject<List<Coin>>(await response.Content.ReadAsStringAsync());
        }
    }
}
