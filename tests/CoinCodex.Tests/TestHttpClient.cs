using CoinCodex.Http;
using CoinCodex.Model;
using NUnit.Framework;
using Moq;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using Moq.Protected;
using System.Threading;
using System.Collections.Specialized;

namespace CoinCodex.Tests
{
    public class ClientTest
    {
        CoinCodexHttpClient _client;
        IConfigReader _configReader;

        string _allCoinsContent;

        [SetUp]
        public void Setup()
        {
            using var reader = new StreamReader(TestContext.CurrentContext.TestDirectory + "\\testdata\\all_coins.json");
            _allCoinsContent = reader.ReadToEnd();

            var appSettings = new NameValueCollection();
            appSettings.Add("BaseUrl", "https://coincodex.com");
            appSettings.Add("GetAllCoinsUrl", "apps/coincodex/cache/all_coins.json");

            _configReader = Mock.Of<IConfigReader>();
            Mock.Get(_configReader)
                .Setup(cr => cr.ReadConfig())
                .Returns(appSettings);
        }
            

        [Test]
        public async Task TestGetAllCoinsAsync()
        {
            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Content = new StringContent(_allCoinsContent)
            };

            handlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(response);

            var httpClient = new HttpClient(handlerMock.Object);
            _client = new CoinCodexHttpClient(httpClient, configReader: _configReader);

            var allCoins = (List<Coin>) await _client.GetAllCoinsAsync();
            Assert.NotNull(allCoins);
            Assert.IsTrue(allCoins.Count > 0);
        }
    }
}